var React = require('react');
var ReactDOM = require('react-dom');
var Main = require('Main');

ReactDOM.render(
    <Main></Main>,
    document.getElementById('app')
);
