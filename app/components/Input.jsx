var React = require('react');

var errors = {
    valid: false,
    hasUpperCase: false,
    hasLowerCase: false,
    hasNumber: false,
    greaterThanEight: false,
};

var Input = React.createClass({

    getInitialState: function () {
        return ({
            type: 'password',
        });
    },

    checkPassword: function (errors, text) {
        var upperCase = /^(?=.*[A-Z]).+$/;
        var lowerCase = /^(?=.*[a-z]).+$/;
        var number = /^(?=.*[0-9]).+$/;

        if (text.length >= 8) {
            errors.greaterThanEight = true;
        } else {
            errors.greaterThanEight = false;
        }

        if (upperCase.test(text)) {
            errors.hasUpperCase = true;
        }
        else {
            errors.hasUpperCase = false;
        }
        if (lowerCase.test(text)) {
            errors.hasLowerCase = true;
        }
        else {
            errors.hasLowerCase = false;
        }
        if (number.test(text)) {
            errors.hasNumber = true;
        }
        else {
            errors.hasNumber = false;
        }
        if (errors.greaterThanEight && errors.hasNumber && errors.hasLowerCase && errors.hasUpperCase)
            errors.valid = true;
        else {
            errors.valid = false;
        }

        return errors;
    },

    checkInputPassword: function (e) {
        var newErrors = this.checkPassword(errors, e.target.value);
        this.props.onChange(newErrors);
    },
    generatePassword: function () {
        var passText = "";
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";


        for (var i = 0; i < Math.floor(Math.random() * 20) + 8; i++) {
            passText += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        return passText;

    },

    setPassword: function () {
        var password = this.generatePassword();
        this.refs.passwordField.value = password;
        var newErrors = this.checkPassword(errors,password);
        this.props.onChange(newErrors);
    },


    setPassType: function () {
        if (this.state.type == 'password') {
            this.setState({
                type: 'text',
            });
        } else {
            this.setState({
                type: 'password',
            });
        }
    },
    render: function () {
        return (
            <div>
                <h4>Enter Password</h4>
                <input ref="passwordField" onChange={this.checkInputPassword} type={this.state.type}/>
                <button onClick={this.setPassType}><i className="glyphicon glyphicon-eye-open"></i></button>
                <br/>
                <button ref="generateButton" onClick={this.setPassword}>Generate Password</button>
            </div>
        );
    },

});
module.exports = Input;