var React = require('react');

var UpperCase = React.createClass(
    {
        render: function () {
            if (!this.props.upper)
                return (<h5>Must contain 1 UpperCase</h5>);
            return null;
        },
    }
);
var LowerCase = React.createClass(
    {
        render: function () {
            if (!this.props.lower)
                return (<h5>Must contain 1 LowerCase</h5>);
            return null;
        },
    }
);
var Number = React.createClass(
    {
        render: function () {
            if (!this.props.numbers)
                return (<h5>Must contain 1 Number</h5>);
            return null;
        },
    }
);
var Length = React.createClass(
    {
        render: function () {
            if (!this.props.length)
                return (<h5>Must contain atleast 8 characters</h5>);
            return null;
        },
    }
);


var Errors = React.createClass({
    render: function () {
        return (
            <div>
                <UpperCase upper={this.props.conditions.hasUpperCase}></UpperCase>
                <LowerCase lower={this.props.conditions.hasLowerCase}></LowerCase>
                <Number numbers={this.props.conditions.hasNumber}></Number>
                <Length length={this.props.conditions.greaterThanEight}></Length>
            </div>
        );
    },
});

module.exports = Errors;