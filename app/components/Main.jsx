var React = require('react');
var Input = require('Input');
var Errors = require('Errors');


var SaveButton = React.createClass({
    render: function () {
        if (this.props.valid) {
            return <button ref="saveButton" onClick={this.save}>Save</button>;
        }
        else return <button disabled ref="saveButton" onClick={this.save}>Save</button>;
    },
});

var Main = React.createClass({

    setConditions: function (state) {
        this.setState(state);
    },
    getInitialState: function () {
        return ({
            valid: false,
            hasUpperCase: false,
            hasLowerCase: false,
            hasNumber: false,
            greaterThanEight: false,
        });
    },
    handelChange: function (errors) {

        this.setState({
            valid: errors.valid,
            hasUpperCase: errors.hasUpperCase,
            hasLowerCase: errors.hasLowerCase,
            hasNumber: errors.hasNumber,
            greaterThanEight: errors.greaterThanEight,
        });

    },

    render: function () {

        return (
            <div className="container-fluid">
                <Input onChange={this.setConditions}/>
                <SaveButton valid={this.state.valid}></SaveButton>
                <Errors conditions={this.state}></Errors>

            </div>
        );
    },

});
module.exports = Main;