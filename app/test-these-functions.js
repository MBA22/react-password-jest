module.exports.generatePassword = function () {
    var passText = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";


    for (var i = 0; i < Math.floor(Math.random() * 20) + 8; i++) {
        passText += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return passText;
}
module.exports.checkPassword = function (errors, text) {
    var upperCase = /^(?=.*[A-Z]).+$/;
    var lowerCase = /^(?=.*[a-z]).+$/;
    var number = /^(?=.*[0-9]).+$/;

    if (text.length >= 8) {
        errors.greaterThanEight = true;
    } else {
        errors.greaterThanEight = false;
    }

    if (upperCase.test(text)) {
        errors.hasUpperCase = true;
    }
    else {
        errors.hasUpperCase = false;
    }
    if (lowerCase.test(text)) {
        errors.hasLowerCase = true;
    }
    else {
        errors.hasLowerCase = false;
    }
    if (number.test(text)) {
        errors.hasNumber = true;
    }
    else {
        errors.hasNumber = false;
    }
    if (errors.greaterThanEight && errors.hasNumber && errors.hasLowerCase && errors.hasUpperCase)
        errors.valid = true;
    else {
        errors.valid = false;
    }

    return errors;
}