var functions = require('../app/test-these-functions');

test('generatePassword returns a password', () => {

    var text = functions.generatePassword();
    var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    expect(text).toEqual(expect.stringMatching(passwordRegex));
});

test('checkPassword checks the errorsObjects and text', () => {


    var errors = {
        valid: false,
        hasUpperCase: false,
        hasLowerCase: false,
        hasNumber: false,
        greaterThanEight: false,
    };
    var text = "Asadsnadnmad21";
    var newErrors = functions.checkPassword(errors, text);

    expect(newErrors).toEqual({
        valid: true,
        hasUpperCase: true,
        hasLowerCase: true,
        hasNumber: true,
        greaterThanEight: true,
    });
});
